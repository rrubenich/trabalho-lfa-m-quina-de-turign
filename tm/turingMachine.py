#!/usr/bin/python
# coding=utf-8


class TuringMachine:

	def turingMachine(self, tm, entry):
		# tm[0] é o alfabeto de entrada
		inputAlphabet = tm[0]
		# tm[1] é o alfabeto da fita
		tape = tm[1]
		# tm[2] é o simbolo inicial 
		initialState = tm[2]
		# tm[3] é uma lista com os estados finais
		finalState = tm[3]
		# tm[4] é o dict das transições
		transictions = tm[4]

		# Variaveis auxiliares para a MT
		atualPosition = 0
		atualState = initialState
		atualDirection = None
		atualSimb = None
		run = True

		while run:
			# Acha as transicao do estado atual
			for transiction in transictions[atualState]:

				# Acha a transição do simbolo lido da fita
				if transiction[0] == entry[atualPosition]:

					# Escreve o estado na fita, imprime e remove o estado
					entry.insert(atualPosition, "["+atualState+"]")
					print ''.join(entry)
					entry.pop(atualPosition)

					#Captura informações da transição
					atualSimb 		= transiction[2]
					atualDirection 	= transiction[3]
					atualState 		= transiction[1]

					# Grava o simbolo (gravado)
					entry[atualPosition] = atualSimb

					# Manipula a direção da cabeça de leitura
					if atualDirection == "D":
						atualPosition+=1
					else:
						atualPosition-=1


					# Caso a cabeça de leitura vá para uma posição antes da fita,
					# a palavra não é aceita
					if atualPosition < 0:
						entry.insert(atualPosition+1, "["+atualState+"]")
						print ''.join(entry), "- não aceita"
						run = False;


					# Se está no estado final a palavra é aceita
					if atualState in finalState:
						# Escreve o estado na fita, e imprime que foi aceita
						entry.insert(atualPosition, "["+atualState+"]")
						print ''.join(entry), "- aceita"
						run = False;


					# Se não é o final, verifica se chegou ao fim da fita, se sim, 
					# adiciona um simbolo branco para evitar um erro (string não é infinita)
					elif atualPosition == len(entry):
						entry.append("B")

					break

			# Não existe transição do estado atual com o simbolo lido, a máquina 
			# para e a entrada não é aceita
			else:					
				entry.insert(atualPosition, "["+atualState+"]")
				print ''.join(entry), "- não aceita"
				run = False
