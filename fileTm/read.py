#!/usr/bin/python
# coding=utf-8

import string
import re

class Read:

	def readFile(self, fileToRead):
		try:
			file  = open(fileToRead, 'r');
			lines = file.readlines()
			file.close()
			return lines
		except:
			print "Arquivo não existe"


	def readTM(self, lines):
		inputAlphabet = []
		tape = []
		initialState = None
		finalState = []
		transictions = {}
		tm = []

		regex = re.compile(":.+")

		# Lê o alfabeto de entrada
		inputAlphabet = regex.findall(lines[0])[0].replace(": ","").split(",")
		del lines[0]

		# Lê o alfabeto da fita
		tape = regex.findall(lines[0])[0].replace(": ","").split(",")
		del lines[0]

		# Lê o estado inicial 
		initialState = regex.findall(lines[0])[0].replace(": ","")
		del lines[0]

		# Lê o estado final 
		finalState = regex.findall(lines[0])[0].replace(": ","").split(",")
		del lines[0]


		# Lê transicoes
		while(lines):
			transParts = lines[0].replace("\n","").split(":")
			transParts = transParts[0].split(',') + transParts[1].split(',')
			
			key = transParts[0]
			del(transParts[0])

			if not key in transictions.keys():
				transictions.update({key:[transParts]})
			else:
				transictions[key].append(transParts)

			del(lines[0])


		for final in finalState:
			if not final in transictions.keys():
				transictions.update({final:[]})

		# tm[0] é o alfabeto de entrada
		tm.append(inputAlphabet)
		# tm[1] é o alfabeto da fita
		tm.append(tape)
		# tm[2] é o simbolo inicial 
		tm.append(initialState)
		# tm[3] é uma lista com os estados finais
		tm.append(finalState)
		# tm[4] é o dict das transições
		# 
		# ['a', simbolo lido
		# 'q1', proximo estado
		# 'X', simbolo gravado
		# 'D'] sentido
		tm.append(transictions)

		return tm