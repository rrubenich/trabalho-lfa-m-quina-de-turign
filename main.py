#!/usr/bin/python
# coding=utf-8




import fileTm.read
import tm.turingMachine
import os


read 	= fileTm.read.Read()
machine = tm.turingMachine.TuringMachine()

fileToRead = None
entry = []


# Lê o arquivo
fileToRead = raw_input("Caminho do arquivo: ")

while not os.path.exists(fileToRead):
	print "Caminho inválido!"
	fileToRead = raw_input("Caminho do arquivo: ")

lines = read.readFile(fileToRead)
tm 	= read.readTM(lines)

# Lê a palavra
entry[:0] = raw_input("Palavra: ")

# Cria a máquina de turing
machine.turingMachine(tm, entry)